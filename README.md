# Cubicle

![Screeshot](/img/cubicle.png)

Simple game engine/framework written in Rust as a proof-of-concept. It utilizes my other library Ruring for
procedural generation of 3D models, and Fiber library as an Entity Component framework. 

The framework also features very simple collision detection algorithm. 

The most interesting experiment is the rendering pipeline - all objects are rendered at once,
although they are represented by different models. This is done by embedding object Id
into each vertex of each model. Then this Id is used to look up transformation matrix from
texture with all transformation matrices. 

The limits of this approach are obvious - Models cannot be reused across objects, 
all models must be loaded in single VBO at start and stay there, and all objects
must use the same shaders. However, the first problem can be mitigated by having another pipeline 
specifically for cases where one model is used many times. 
It has a potential to render large number of simple objects quickly.

The project is built for browser ecosystem, so the result is a wasm module.
