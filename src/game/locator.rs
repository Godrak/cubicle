use super::geometry::BoundingBox;
use cgmath::prelude::*;

pub struct Locator<T> {
    volume: Vec<T>,
    pub size: cgmath::Vector3<u32>,
    pub bounding_box: BoundingBox,
    pub voxel_size: cgmath::Vector3<f32>,
}

impl<T: Default> Locator<T> {
    pub fn new(size: cgmath::Vector3<u32>, bounding_box: BoundingBox) -> Self {
        let mut volume = Vec::<T>::new();
        for _z in 0..size.z {
            for _y in 0..size.y {
                for _x in 0..size.x {
                    volume.push(T::default());
                }
            }
        }
        let voxel_size = bounding_box.size().div_element_wise(size.cast().unwrap());

        Locator {
            volume,
            size,
            bounding_box,
            voxel_size,
        }
    }

    fn to_linear_index(&self, coords: &cgmath::Vector3<i32>) -> i32 {
        coords.z * (self.size.y * self.size.x) as i32 + coords.y * self.size.x as i32 + coords.x
    }

    pub fn world_to_local(&self, world_coords: &cgmath::Vector3<f32>) -> cgmath::Vector3<i32> {
        world_coords
            .sub_element_wise(self.bounding_box.min_corner)
            .div_element_wise(self.voxel_size)
            .map(|i| (i.floor()) as i32)
    }

    pub fn access_mut(&mut self, coords: &cgmath::Vector3<i32>) -> Option<&mut T> {
        let li = self.to_linear_index(coords);
        if li >= 0 {
            self.volume.get_mut(li as usize)
        } else {
            None
        }
    }

    pub fn access(&self, coords: &cgmath::Vector3<i32>) -> Option<&T> {
        let li = self.to_linear_index(coords);
        if li >= 0 {
            self.volume.get(li as usize)
        } else {
            None
        }
    }

    pub fn get_voxel_bounding_box(&self, coords: &cgmath::Vector3<i32>) -> BoundingBox {
        let voxel_start = self.bounding_box.min_corner + coords.cast().unwrap().mul_element_wise(self.voxel_size);
        BoundingBox::new(voxel_start, voxel_start + self.voxel_size)
    }
}
