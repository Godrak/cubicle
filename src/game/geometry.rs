use cgmath::prelude::*;
use cgmath::Vector3;
use std::cmp::min;
use cgmath::num_traits::abs;
use std::cmp::PartialOrd;

#[derive(Clone, Copy, Debug)]
pub struct BoundingBox {
    pub min_corner: cgmath::Vector3<f32>,
    pub max_corner: cgmath::Vector3<f32>,
}

impl BoundingBox {
    pub fn new(min_corner: cgmath::Vector3<f32>, max_corner: cgmath::Vector3<f32>) -> Self {
        BoundingBox {
            min_corner,
            max_corner,
        }
    }

    pub fn size(&self) -> cgmath::Vector3<f32> {
        self.max_corner - self.min_corner
    }

    pub fn center(&self) -> cgmath::Vector3<f32> {
        (self.max_corner - self.min_corner) / 2.0
    }


    pub fn is_inside(&self, point: cgmath::Vector3<f32>) -> bool {
        (0..3).all(|i| self.min_corner[i] <= point[i] && self.max_corner[i] >= point[i])
    }

    pub fn transformed(&self, transform: &cgmath::Vector3<f32>) -> Self {
        return BoundingBox::new(&self.min_corner + transform, &self.max_corner + transform);
    }

    pub fn intersection_shift(&self, other: &BoundingBox) -> Option<Vector3<f32>> {
        let x_intersect = self.min_corner.x < other.max_corner.x && self.max_corner.x > other.min_corner.x;
        let x_shift = BoundingBox::min_in_abs(&other.max_corner.x - &self.min_corner.x, &other.min_corner.x - &self.max_corner.x);

        let y_intersect = self.min_corner.y < other.max_corner.y && self.max_corner.y > other.min_corner.y;
        let y_shift = BoundingBox::min_in_abs(&other.max_corner.y - &self.min_corner.y, &other.min_corner.y - &self.max_corner.y);

        let z_intersect = self.min_corner.z < other.max_corner.z && self.max_corner.z > other.min_corner.z;
        let z_shift = BoundingBox::min_in_abs(&other.max_corner.z - &self.min_corner.z, &other.min_corner.z - &self.max_corner.z);

        return if x_intersect && y_intersect && z_intersect {
            let v = cgmath::vec3(x_shift, y_shift, z_shift);
            let i = BoundingBox::index_of_min_in_abs(v);
            let mut res = cgmath::vec3(0.0, 0.0, 0.0);
            res[i] = v[i];
            Some(res)
        } else {
            None
        };
    }

    fn min_in_abs(x: f32, y: f32) -> f32 {
        return if abs(x) < abs(y) {
            x.clone()
        } else {
            y.clone()
        };
    }

    fn index_of_min_in_abs(v: cgmath::Vector3<f32>) -> usize {
        return if abs(v.x) < abs(v.y) {
            if abs(v.z) < abs(v.x) {
                2
            } else {
                0
            }
        } else {
            if abs(v.z) < abs(v.y) {
                2
            } else {
                1
            }
        };
    }
}

#[derive(Clone, Copy)]
pub struct BoundingSphere {
    pub center: cgmath::Vector3<f32>,
    pub radius: f32,
}


impl BoundingSphere {
    pub fn new(center: cgmath::Vector3<f32>, radius: f32) -> Self {
        BoundingSphere {
            center,
            radius,
        }
    }

    pub fn is_inside(&self, point: cgmath::Vector3<f32>) -> bool {
        self.center.distance(point) <= self.radius
    }
}
