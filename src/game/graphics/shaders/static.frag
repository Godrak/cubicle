
precision mediump float;

in vec3 v_color;
in vec3 v_position;
in vec3 v_normal;

const vec3 light_color = vec3(1.0,1.0,1.0);
const vec3 light_pos = vec3(0.0, 50.0, 30.0);
const vec3 ambient_color = vec3(0.3,0.3,0.3);

out vec4 frag_color;

void main()
{
    vec3 light_dir = light_pos - v_position;
    float light_distance = length(light_dir);
    light_dir = normalize(light_dir);
    float distance2 = light_distance*light_distance;

    float lambertian = max(dot(light_dir, v_normal),0.0);

    vec3 color = ambient_color*v_color + light_color*v_color*lambertian;

    frag_color = vec4(color,1);
}
