layout (location = 0) in uint transform_id;
layout (location = 1) in vec3 position;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 color;

uniform mat4 camera;
uniform sampler2D transforms;

out vec3 v_color;
out vec3 v_position;
out vec3 v_normal;

//mat4 get_transform(){
//    vec4 c0 = texelFetch(transforms, ivec2(0, transform_id), 0);
//    vec4 c1 = texelFetch(transforms, ivec2(1, transform_id), 0);
//    vec4 c2 = texelFetch(transforms, ivec2(2, transform_id), 0);
//    vec4 c3 = texelFetch(transforms, ivec2(3, transform_id), 0);
//    return mat4(c0, c1, c2, c3);
//}

vec3 get_world_position(){
    vec4 c0 = texelFetch(transforms, ivec2(0, transform_id), 0);
    return c0.xyz;
}

vec3 get_world_direction(){
    vec4 c1 = texelFetch(transforms, ivec2(1, transform_id), 0);
    return c1.xyz;
}

mat4 rotation_matrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
    oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
    oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
    0.0, 0.0, 0.0, 1.0);
}

mat4 create_vector_alignment_matrix(vec3 dir, vec3 target_dir){
    vec3 start = normalize(dir);
    vec3 end = normalize(target_dir);

    if (length(start-end) < 1e-4) {
        return mat4(1.0);
    } else if (length(start+end) < 1e-4){
        vec3 axis = vec3(0,0,1);
        float angle = 3.14;
        return rotation_matrix(axis, angle);
    }
    else {
        vec3 axis = normalize(cross(start, end));
        float cos = dot(start, end);
        float angle = acos(cos);
        return rotation_matrix(axis, angle);
    }
}

mat4 from_translation(vec3 translation){
    return mat4(
    vec4(1, 0, 0, 0),
    vec4(0, 1, 0, 0),
    vec4(0, 0, 1, 0),
    vec4(translation, 1)
    );
}

void main() {
    vec3 world_pos = get_world_position();
    vec3 world_dir = get_world_direction();
    mat4 translation = from_translation(world_pos);
    mat4 rotation = create_vector_alignment_matrix(vec3(0, 1, 0), world_dir);

    v_color = color;
    v_position = position;
    v_normal = normal;
    gl_Position = camera*translation*rotation*vec4(position, 1.0);
}
