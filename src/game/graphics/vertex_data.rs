use super::vertex::Vertex;
use super::GraphicsParams;
use glow::*;
use std::mem;
use std::slice;

pub struct VertexData {
    pub vertex_array: glow::VertexArray,
    pub vertex_buffer: glow::Buffer,
    pub transforms_tex: glow::Texture,
    local_vertex_buffer: Vec<Vertex>,
}

impl VertexData {
    pub fn new(params: &GraphicsParams) -> Self {
        let gl = &params.gl;
        let vertex_array = unsafe { gl.create_vertex_array() }.expect("Cannot create vertex array");
        unsafe {
            gl.bind_vertex_array(Some(vertex_array));
        }

        let vertex_buffer = unsafe { gl.create_buffer() }.expect("Cannot create vertex buffer");
        unsafe {
            gl.bind_buffer(glow::ARRAY_BUFFER, Some(vertex_buffer));
        }
        Vertex::vertex_attrib_pointers_init(gl);

        let transforms_tex = unsafe { gl.create_texture() }.expect("Cannot create transforms");
        unsafe {
            gl.active_texture(glow::TEXTURE0);
            gl.bind_texture(glow::TEXTURE_2D, Some(transforms_tex));
            gl.tex_storage_2d(glow::TEXTURE_2D, 1, glow::RGB32F, 2, 500);
            // gl.tex_image_2d(glow::TEXTURE_2D, 0, glow::RGBA32F as i32, 500, 4, 0, glow::RGBA, glow::FLOAT, None);

            gl.tex_parameter_i32(
                glow::TEXTURE_2D,
                glow::TEXTURE_WRAP_S,
                glow::CLAMP_TO_EDGE as i32,
            );
            gl.tex_parameter_i32(
                glow::TEXTURE_2D,
                glow::TEXTURE_WRAP_T,
                glow::CLAMP_TO_EDGE as i32,
            );
            gl.tex_parameter_i32(
                glow::TEXTURE_2D,
                glow::TEXTURE_MIN_FILTER,
                glow::NEAREST as i32,
            );
            gl.tex_parameter_i32(
                glow::TEXTURE_2D,
                glow::TEXTURE_MAG_FILTER,
                glow::NEAREST as i32,
            );
        }

        unsafe {
            gl.bind_vertex_array(None);
        };

        VertexData {
            vertex_array,
            vertex_buffer,
            transforms_tex,
            local_vertex_buffer: vec![],
        }
    }

    pub fn add_data_local(&mut self, data: &[Vertex]) {
        self.local_vertex_buffer.extend(data.iter());
    }

    pub fn buffer_data(&self, gl: &glow::Context) {
        unsafe {
            gl.bind_vertex_array(Some(self.vertex_array));
            gl.bind_buffer(glow::ARRAY_BUFFER, Some(self.vertex_buffer));

            let data_pointer: *const u8 = &self.local_vertex_buffer[0] as *const _ as *const u8; //first cast to pointer then to pointer to bytes, cant cast directly
            let data_buffer_view: &[u8] = slice::from_raw_parts(
                data_pointer,
                mem::size_of::<Vertex>() * self.local_vertex_buffer.len(),
            );

            gl.buffer_data_u8_slice(glow::ARRAY_BUFFER, data_buffer_view, glow::STATIC_DRAW);
        }
    }

    pub fn vertex_count(&self) -> usize {
        self.local_vertex_buffer.len()
    }
}
