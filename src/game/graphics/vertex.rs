use super::*;

#[derive(Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct Vertex {
    transform_id: u32,
    position: cgmath::Vector3<f32>,
    normal: cgmath::Vector3<f32>,
    color: cgmath::Vector3<f32>,
}

impl Vertex {
    pub fn new(
        transform_id: u32,
        position: cgmath::Vector3<f32>,
        normal: cgmath::Vector3<f32>,
        color: cgmath::Vector3<f32>,
    ) -> Self {
        Vertex {
            transform_id,
            position,
            normal,
            color,
        }
    }

    pub fn vertex_attrib_pointers_init(gl: &glow::Context) {
        unsafe {
            //enable transform_id
            gl.enable_vertex_attrib_array(0);
            gl.vertex_attrib_pointer_i32(
                0,
                1,
                glow::UNSIGNED_INT,
                std::mem::size_of::<Vertex>() as i32,
                0,
            );

            //enable position
            gl.enable_vertex_attrib_array(1);
            gl.vertex_attrib_pointer_f32(
                1,
                3,
                glow::FLOAT,
                false,
                std::mem::size_of::<Vertex>() as i32,
                std::mem::size_of::<u32>() as i32,
            );

            //enable normal
            gl.enable_vertex_attrib_array(2);
            gl.vertex_attrib_pointer_f32(
                2,
                3,
                glow::FLOAT,
                true,
                std::mem::size_of::<Vertex>() as i32,
                (std::mem::size_of::<u32>() + std::mem::size_of::<cgmath::Vector3<f32>>()) as i32,
            );

            //enable color
            gl.enable_vertex_attrib_array(3);
            gl.vertex_attrib_pointer_f32(
                3,
                3,
                glow::FLOAT,
                true,
                std::mem::size_of::<Vertex>() as i32,
                (std::mem::size_of::<u32>() + 2 * std::mem::size_of::<cgmath::Vector3<f32>>())
                    as i32,
            );
        }
    }
}
