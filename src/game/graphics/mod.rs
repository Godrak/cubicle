use glow::*;

pub mod camera;
pub mod vertex;
pub mod vertex_data;
pub mod viewport;

use camera::Camera;
pub use vertex_data::VertexData;
use viewport::Viewport;

pub struct GraphicsParams {
    pub gl: glow::Context,
    pub shader_version: &'static str,
    pub canvas_size: (i32, i32),
}

pub struct Graphics {
    pub gl: glow::Context,
    shader_version: &'static str,
    camera: Camera,
    viewport: Viewport,
    shader_program: glow::Program,
    camera_location: glow::UniformLocation,
    transforms_location: glow::UniformLocation,
}

impl Graphics {
    pub fn new(params: GraphicsParams) -> Self {
        let vert_source = include_str!("shaders/static.vert");
        let frag_source = include_str!("shaders/static.frag");
        let program = build_program(&params, vert_source, frag_source);

        let gl = params.gl;
        let camera_location = unsafe { gl.get_uniform_location(program, "camera") }
            .expect("could not retrieve camera uniform location");

        let transforms_location = unsafe { gl.get_uniform_location(program, "transforms") }
            .expect("could not retrieve transforms uniform location");

        let camera = Camera::new();
        let (width, height) = params.canvas_size;
        let viewport = Viewport::new(width, height);

        unsafe {
            gl.clear_color(1.0, 1.0, 1.0, 1.0);
            gl.enable(glow::DEPTH_TEST);
            gl.enable(glow::CULL_FACE);
            gl.clear(glow::COLOR_BUFFER_BIT);
            gl.clear(glow::DEPTH_BUFFER_BIT);
            viewport.set_used(&gl);
        };
        Graphics {
            gl,
            shader_version: params.shader_version,
            camera,
            viewport,
            shader_program: program,
            camera_location,
            transforms_location,
        }
    }

    pub fn clear(&self) {
        unsafe {
            self.gl.clear(glow::COLOR_BUFFER_BIT);
            self.gl.clear(glow::DEPTH_BUFFER_BIT);
        }
    }

    pub fn resize_viewport(&mut self, new_size: cgmath::Vector2<i32>) {
        self.viewport.update_size(new_size.x, new_size.y);
        self.viewport.set_used(&self.gl);
    }

    pub fn render(&self, vertex_data: &VertexData, transform_data: &[u8]) {
        let camera_matrix = self.camera.get_transform_matrix(&self.viewport);
        let m: &[f32; 16] = camera_matrix.as_ref();
        let gl = &self.gl;

        unsafe {
            gl.bind_vertex_array(Some(vertex_data.vertex_array));
            gl.tex_sub_image_2d(
                glow::TEXTURE_2D,
                0,
                0,
                0,
                2,
                (transform_data.len() / 24) as i32,
                glow::RGB,
                glow::FLOAT,
                glow::PixelUnpackData::Slice(transform_data),
            );
            gl.use_program(Some(self.shader_program));
            gl.uniform_matrix_4_f32_slice(Some(&self.camera_location), false, m);
            gl.uniform_1_i32(Some(&self.transforms_location), 0);
            gl.draw_arrays(glow::TRIANGLES, 0, vertex_data.vertex_count() as i32);
        }
    }
}

pub fn build_program(
    params: &GraphicsParams,
    vert_source: &str,
    frag_source: &str,
) -> glow::Program {
    let gl = &params.gl;
    let program = unsafe { gl.create_program() }.expect("Cannot create program");

    let vert_shader = unsafe {
        let shader = gl
            .create_shader(glow::VERTEX_SHADER)
            .expect("Cannot create shader");
        gl.shader_source(
            shader,
            &format!("{}\n{}", params.shader_version, vert_source),
        );
        gl.compile_shader(shader);

        if !gl.get_shader_compile_status(shader) {
            panic!("{}", gl.get_shader_info_log(shader));
        }
        gl.attach_shader(program, shader);
        shader
    };

    let frag_shader = unsafe {
        let shader = gl
            .create_shader(glow::FRAGMENT_SHADER)
            .expect("Cannot create shader");
        gl.shader_source(
            shader,
            &format!("{}\n{}", params.shader_version, frag_source),
        );
        gl.compile_shader(shader);

        if !gl.get_shader_compile_status(shader) {
            panic!("{}", gl.get_shader_info_log(shader));
        }
        gl.attach_shader(program, shader);
        shader
    };

    unsafe {
        gl.link_program(program);
        if !gl.get_program_link_status(program) {
            panic!("{}", gl.get_program_info_log(program));
        }
        gl.detach_shader(program, vert_shader);
        gl.delete_shader(vert_shader);
        gl.detach_shader(program, frag_shader);
        gl.delete_shader(frag_shader);
        gl.use_program(Some(program));
    }
    program
}
