use super::viewport::Viewport;

pub struct Camera {
    position: cgmath::Point3<f32>,
    direction: cgmath::Vector3<f32>,
    up: cgmath::Vector3<f32>,
    step_size: f32,
}

impl Camera {
    pub fn new() -> Self {
        let position = cgmath::Point3 {
            x: 15.0,
            y: 15.0,
            z: 30.0,
        };
        let direction = cgmath::Vector3 {
            x: 0.0,
            y: 0.0,
            z: -1.0,
        };
        let up = cgmath::Vector3 {
            x: 0.0,
            y: 1.0,
            z: 0.0,
        };
        let step_size = 9.0 / 60.0;
        Camera {
            position,
            direction,
            up,
            step_size,
        }
    }

    pub fn get_transform_matrix(&self, viewport: &Viewport) -> cgmath::Matrix4<f32> {
        let aspect = viewport.w as f32 / viewport.h as f32;
        let view_t = cgmath::Matrix4::look_to_rh(self.position, self.direction, self.up);
        let proj_t = cgmath::perspective(cgmath::Deg(60.0), aspect, 0.001, 1000.0);

        proj_t * view_t
    }

    // pub fn move_camera(&mut self, key: &glfw::Key) -> () {
    //     match key {
    //         glfw::Key::W => self.position += self.step_size * self.direction,
    //         glfw::Key::S => self.position -= self.step_size * self.direction,
    //         glfw::Key::A => self.position -= self.direction.cross(self.up) * self.step_size,
    //         glfw::Key::D => self.position += self.direction.cross(self.up) * self.step_size,
    //         glfw::Key::Q => self.position += self.up * self.step_size,
    //         glfw::Key::E => self.position -= self.up * self.step_size,
    //         _ => (),
    //     };
    //     self.up = self
    //         .direction
    //         .cross(m::Vector3::unit_z())
    //         .cross(self.direction);
    // }

    // pub fn rotate_camera(&mut self, offset: m::Vector2<f32>) {
    //     let mut step = self.direction.cross(self.up).normalize();
    //     step *= offset[0];
    //     self.direction = (self.direction - step).normalize();
    //     step = self.up * offset[1];
    //     self.direction = (self.direction - step).normalize();
    //     self.up = (self.direction.cross(m::Vector3::unit_z())).cross(self.direction);
    // }
}
