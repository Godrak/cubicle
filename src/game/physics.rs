use cgmath::*;
use fiber::*;
use super::geometry::*;
use super::locator::*;
use seed::app::RenderInfo;
use seed::prelude::web_sys::console::log_1;
use std::collections::HashSet;
use rand::Rng;
use rand::SeedableRng;
use rand_pcg::*;
use crate::game::components::*;
use super::Events;
use crate::game::Trigger::{PhysicsConflict, PositionUpdate};

pub struct Physics {
    locator: Locator<HashSet<EntityId>>,
    rng: Pcg32,
}

impl Physics {
    pub fn new(size: cgmath::Vector3<u32>, map_bounding_box: BoundingBox) -> Self {
        let locator = Locator::new(size, map_bounding_box);
        return Physics {
            locator,
            rng: Pcg32::seed_from_u64(0),
        };
    }

    pub fn remove(&mut self, fiber: &mut Fiber, events: &mut Events, id: EntityId) {
        if let Ok((body, transform)) = fiber.borrow_mut2::<PhysicsBody, Transformation>(id) {
            let indices = self.body_to_indices(&body.body.transformed(&transform.position));
            for i in indices {
                if let Some(hmap) = self.locator.access_mut(&i) {
                    hmap.remove(&id);
                }
            }
        }
    }

    pub fn add(&mut self, fiber: &mut Fiber, events: &mut Events, id: EntityId) {
        self.remove(fiber, events, id);
        if let Ok((body, transform)) = fiber.borrow_mut2::<PhysicsBody, Transformation>(id) {
            let indices = self.body_to_indices(&body.body.transformed(&transform.position));
            for i in indices {
                if let Some(hmap) = self.locator.access_mut(&i) {
                    hmap.insert(id);
                }
            }
        }
    }

    pub fn react_to_desired_position_update(&mut self, fiber: &mut Fiber, events: &mut Events, id: EntityId, new_pos: Vector3<f32>) {
        let mut body_opt = None;
        if let Ok((body, transform)) = fiber.borrow_mut2::<PhysicsBody, Transformation>(id) {
            let indices = self.body_to_indices(&body.body.transformed(&transform.position));
            for i in indices {
                if let Some(hmap) = self.locator.access_mut(&i) {
                    hmap.remove(&id);
                }
            }

            body_opt = Some(body.clone());
        }

        if let Some(body) = body_opt {
            let new_pos = self.resolve_conflicts(fiber, events, id, &body, new_pos);
            fiber.borrow_mut::<Transformation>(id).unwrap().position = new_pos;
            self.add(fiber, events, id);
        }
    }

    fn body_to_indices(&self, body: &BoundingBox) -> Vec<Vector3<i32>> {
        let min = self.locator.world_to_local(&body.min_corner);
        let max = self.locator.world_to_local(&body.max_corner);
        let mut result: Vec<Vector3<i32>> = vec! {};
        for x in min.x..=max.x {
            for y in min.y..=max.y {
                for z in min.z..=max.z {
                    result.push(vec3(x.clone(), y.clone(), z.clone()))
                }
            }
        }
        return result;
    }

    fn resolve_conflicts(&mut self, fiber: &Fiber, events: &mut Events, id: EntityId, body: &PhysicsBody, new_pos: Vector3<f32>) -> Vector3<f32> {
        let mut transform = new_pos;
        let indices = self.body_to_indices(&body.body.transformed(&transform));
        let mut entities = HashSet::<EntityId>::default();
        for i in indices {
            if let Some(hmap) = self.locator.access(&i) {
                entities.extend(hmap);
            }
        }

        for e in entities {
            if let Ok(body2) = fiber.borrow::<PhysicsBody>(e) {
                transform = self.resolve_conflict(events, body, &transform, body2, id, e);
            }
        }

        return transform;
    }

    fn resolve_conflict(&mut self, events: &mut Events, body: &PhysicsBody, transform: &Vector3<f32>, static_body: &PhysicsBody, id: EntityId, id_static: EntityId) -> Vector3<f32> {
        let mut transform = transform;
        return if let Some(shift) = body.body.transformed(&transform).intersection_shift(&static_body.body) {
            events.push_back(PhysicsConflict(id, id_static));
            if body.priority > static_body.priority {
                transform + shift
            } else if body.priority == static_body.priority && self.rng.gen_bool(0.5) {
                transform + shift
            } else {
                transform.clone()
            }
        } else { transform.clone() }
    }
}