use cgmath::*;
use fiber::*;
use super::geometry::*;
use seed::app::RenderInfo;
use seed::prelude::web_sys::console::log_1;
use std::collections::HashSet;
use super::Events;
use super::Trigger::*;

////////////////
//  Transformation
/////////////////
#[derive(Clone)]
pub struct Transformation {
    pub position: Vector3<f32>,
    pub direction: Vector3<f32>,
}

impl Transformation {
    pub fn new(position: &Vector3<f32>) -> Self {
        Transformation {
            position: position.clone(),
            direction: vec3(0.0, 1.0, 0.0),
        }
    }

    pub fn new2(position: Vector3<f32>, direction: Vector3<f32>) -> Self {
        Transformation {
            position,
            direction,
        }
    }
}

impl Default for Transformation {
    fn default() -> Self {
        Transformation {
            position: vec3(0.0, 0.0, 0.0),
            direction: vec3(0.0, 1.0, 0.0),
        }
    }
}

////////////////
//  World Tag
/////////////////
#[derive(Clone)]
pub struct WorldTag {
    pub world_id: EntityId,
}

////////////////
//  Keyboard controller
/////////////////

pub struct KeyboardController {
    pub direction: Vector3<f32>,
    pub shooting: bool,
    pub moving: bool,
    keys_down: HashSet<u32>,
    movement_keycodes: HashSet<u32>,
}

impl KeyboardController {
    pub fn new() -> Self {
        KeyboardController {
            direction: vec3(0.0, 1.0, 0.0),
            shooting: false,
            moving: false,
            keys_down: HashSet::new(),
            movement_keycodes: vec![37, 38, 39, 40, 87, 83, 65, 68].into_iter().collect(),
        }
    }

    pub fn react(&mut self, keycode: u32, down: bool) {
        if !down {
            self.keys_down.remove(&keycode);
        } else if !self.keys_down.contains(&keycode) {
            self.keys_down.insert(keycode);
            match keycode {
                38 | 87 => self.direction = vec3(0.0, 1.0, 0.0),
                37 | 65 => self.direction = vec3(-1.0, 0.0, 0.0),
                40 | 83 => self.direction = vec3(0.0, -1.0, 0.0),
                39 | 68 => self.direction = vec3(1.0, 0.0, 0.0),
                _ => {}
            }
        }

        self.shooting = self.keys_down.contains(&32);
        self.moving = self.keys_down.intersection(&self.movement_keycodes).count() > 0;
    }
}

////////////////
//  Character
/////////////////

pub struct Character {
    pub id: EntityId,
    pub alive: bool,
}

impl Character {
    pub fn update(fiber: &mut Fiber, events: &mut Events, delta: f64) {
        let ids = fiber.list1::<Character>();
        // log_1(&format!("ids {}", ids.len()).into());
        // log_1(&format!("timestamp {}", delta.timestamp_delta.unwrap_or(0.0)).into());
        for character_id in ids {
            if let Ok((char, kc, transform)) =
            fiber.borrow_mut3::<Character, KeyboardController, Transformation>(character_id)
            {
                if kc.moving && char.alive {
                    events.push_back(PositionUpdate(character_id, transform.position));
                    let new_position = transform.position + kc.direction * delta as f32 / 100.0;
                    transform.direction = kc.direction;

                    events.push_back(PositionUpdate(character_id, new_position));
                }
            }
        }
    }
}

////////////////
//  Physics body
/////////////////

#[derive(Clone)]
pub struct PhysicsBody {
    pub body: BoundingBox,
    pub priority: u32,
}

impl PhysicsBody {
    pub fn get_transformed(&self, position: &Vector3<f32>) -> Self {
        PhysicsBody {
            body: self.body.transformed(position),
            priority: self.priority.clone(),
        }
    }
}