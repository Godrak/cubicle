use cgmath::*;
use fiber::*;
use rand::SeedableRng;
use std::collections::VecDeque;

mod geometry;
mod locator;
mod components;

use components::*;

mod generation;

mod graphics;
mod physics;

use graphics::*;
use physics::*;
use seed::prelude::web_sys::console::log_1;


pub type Events = VecDeque<Trigger>;

pub struct StartParams {
    pub graphics: GraphicsParams,
}

impl StartParams {
    pub fn new(
        gl: glow::Context,
        shader_version: &'static str,
        canvas_size: (i32, i32),
    ) -> StartParams {
        let graphics = GraphicsParams {
            gl,
            shader_version,
            canvas_size,
        };
        StartParams { graphics }
    }
}

pub struct Game {
    graphics: Graphics,
    physics: Physics,
    fiber: Fiber,
    world_id: EntityId,
    char_id: EntityId,
    events: Events,
    fps: Fps,
}

#[derive(Debug)]
pub enum Trigger {
    Resize(i32, i32),
    KeyDown(u32, bool),
    PositionUpdate(EntityId, Vector3<f32>),
    PhysicsConflict(EntityId, EntityId),
}

impl Game {
    pub fn new(params: StartParams) -> Self {
        //events
        let mut events: Events = VecDeque::default();

        //fiber
        let mut fiber = Fiber::default();
        let world_id = fiber.create_entity();
        let world_tag = WorldTag { world_id };
        fiber.set(world_id, world_tag).unwrap();

        //graphics
        let vertex_data = vertex_data::VertexData::new(&params.graphics);
        let graphics = Graphics::new(params.graphics);

        fiber.set(world_id, vertex_data).unwrap();
        let origin = Transformation::default();
        fiber.set(world_id, origin).unwrap();

        //map
        let map_params = generation::map::MapGenerationParams {
            seed: 1234,
            map_size: vec2(10, 10),
            map_bounding_box: geometry::BoundingBox::new(
                vec3(0.0, 0.0, 0.0),
                vec3(30.0, 30.0, 3.0),
            ),
        };
        generation::map::generate_map(&map_params, &mut fiber);

        let mut physics = Physics::new(vec3(15, 15, 3), geometry::BoundingBox::new(
            vec3(0.0, 0.0, -3.0),
            vec3(30.0, 30.0, 3.0),
        ));

        let char_params = generation::character::CharacterGenerationParams {
            seed: 10,
            color_seed: 10,
            bounding_box: geometry::BoundingBox::new(vec3(-1.0, -1.0, 0.0), vec3(1.0, 1.0, 3.0)),
            position: vec3(10.0, 10.0, 0.0),
        };

        let char_id = generation::character::generate_character(&char_params, &mut fiber);
        fiber.set(char_id, KeyboardController::new());

        fiber
            .borrow_mut::<VertexData>(world_id)
            .unwrap()
            .buffer_data(&graphics.gl);

        for entity_id in fiber.list1::<PhysicsBody>() {
            physics.add(&mut fiber, &mut events, entity_id);
        }

        Game {
            graphics,
            physics,
            fiber,
            world_id,
            char_id,
            events,
            fps: Fps::new(40.0),
        }
    }

    pub fn react(&mut self, action: Trigger) -> () {
        match action {
            Trigger::Resize(width, height) => self
                .graphics
                .resize_viewport(vec2(width.clone(), height.clone())),
            Trigger::KeyDown(keycode, down) => {
                self.fiber.access_all_components_mut::<KeyboardController>()
                    .map(
                        |list| list.iter_mut().for_each(|kc| kc.react(keycode, down))).ok();
            }
            Trigger::PositionUpdate(id, new_pos) => {
                self.physics.react_to_desired_position_update(&mut self.fiber, &mut self.events, id, new_pos);
            }
            Trigger::PhysicsConflict(id1, id2) => {
            }
        }
    }

    pub fn update(&mut self, delta: seed::app::RenderInfo) -> () {
        let (do_update, time_delta) = self.fps.update(delta);
        if do_update {
            //Update
            Character::update(&mut self.fiber, &mut self.events, time_delta);

            //Events
            let mut e = self.events.pop_front();
            while let Some(event) = e {
                // use seed::prelude::web_sys::console::log_1;
                // log_1(&format!("event {:#?}", event).into());
                self.react(event);
                e = self.events.pop_front();
            }

            //Graphics
            self.graphics.clear();
            self.graphics.render(
                self.fiber.borrow::<VertexData>(self.world_id).unwrap(),
                self.fiber.memory_view::<Transformation>().unwrap(),
            );
        }
    }
}


struct Fps {
    fps: f32,
    last_time: f64,
    update_time: f64,
    update_delta: f64,
}

impl Fps {
    pub fn new(fps: f32) -> Self {
        return Fps {
            fps,
            last_time: 0.0,
            update_time: 0.0,
            update_delta: 1000.0 / fps as f64,
        };
    }

    pub fn update(&mut self, delta: seed::app::RenderInfo) -> (bool, f64) {
        let result: (bool, f64);
        if delta.timestamp > self.update_time {
            result = (true, delta.timestamp - self.last_time);
            self.update_time = self.last_time + (delta.timestamp - self.last_time) / 2.0 + self.update_delta;
            self.last_time = delta.timestamp;
        } else { result = (false, delta.timestamp - self.last_time); }

        result
    }
}
