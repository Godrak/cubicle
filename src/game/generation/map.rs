use super::*;

pub struct MapGenerationParams {
    pub seed: u64,
    pub map_size: cgmath::Vector2<u32>,
    pub map_bounding_box: BoundingBox,
}

pub fn generate_map(params: &MapGenerationParams, fiber: &mut Fiber) -> Vec<EntityId> {
    let world_id = fiber
        .access_all_components::<WorldTag>()
        .unwrap()
        .first()
        .unwrap()
        .world_id;

    let mut walls = vec! {};
    let map = generate(&params);
    let mut g = rand_pcg::Pcg32::seed_from_u64(params.seed);
    for x in 0..map.size.x {
        for y in 0..map.size.y {
            if !map.access(&vec3(x as i32, y as i32, 0)).unwrap().space {
                let wall = fiber.create_entity();
                walls.push(wall);
                let c = make_wall(
                    0,
                    &mut g,
                    &map.get_voxel_bounding_box(&vec3(x as i32, y as i32, 0)),
                );
                let body = PhysicsBody {
                    body: map.get_voxel_bounding_box(&vec3(x as i32, y as i32, 0)),
                    priority: 0,
                };
                fiber.set(wall, body);
                let transform = Transformation::default();
                fiber.set(wall, transform);

                fiber.borrow_mut::<VertexData>(world_id).unwrap().add_data_local(&c);
            }
        }
    }

    let floor = fiber.create_entity();
    let floor_area = params.map_bounding_box.transformed(&Vector3::new(0.0, 0.0, -params.map_bounding_box.max_corner.z));
    let c = make_wall(
        0,
        &mut g,
        &floor_area,
    );
    let floor_body = PhysicsBody {
        body: floor_area,
        priority: 0,
    };
    fiber.set(floor, floor_body);
    let transform = Transformation::default();
    fiber.set(floor, transform);

    fiber.borrow_mut::<VertexData>(world_id).unwrap().add_data_local(&c);

    return walls;
}

pub fn make_wall(transform_id: u32, rng: &mut Pcg32, bbox: &BoundingBox) -> Vec<Vertex> {
    let sample = vec![
        bbox.min_corner,
        vec3(bbox.max_corner.x, bbox.min_corner.y, bbox.min_corner.z),
        vec3(bbox.max_corner.x, bbox.max_corner.y, bbox.min_corner.z),
        vec3(bbox.min_corner.x, bbox.max_corner.y, bbox.min_corner.z),
    ];

    let steps = 3;
    let bottom = resample_ring_uniform(&sample, steps * 4, false);
    let mut rings = vec![bottom];
    for i in 1..=steps {
        let ring = translate(
            &rings[0],
            V::new(
                0.0,
                0.0,
                (bbox.max_corner.z - bbox.min_corner.z) / steps as F * i as F,
            ),
        );
        rings.push(ring);
    }
    rings.push(translate_random(
        &scale_from_centroid(rings.last().unwrap(), 0.5),
        V::new(-0.12, -0.0001, -0.12),
        V::new(0.12, 0.00000, 0.12),
        rng,
    ));
    let positions = assemble_mesh(&rings, true, true);
    let normals = compute_normals(&positions);

    positions
        .into_iter()
        .zip(normals.into_iter())
        .map(|(p, n)| Vertex::new(transform_id, p, n, V::new(1.0, 0.0, 0.0)))
        .collect()
}

#[derive(Default)]
struct Tile {
    pub space: bool,
    // pub ceiling: bool,
    // pub obstacle: bool,
}

fn generate(params: &MapGenerationParams) -> Locator<Tile> {
    let mut rng = Pcg32::seed_from_u64(params.seed);
    let mut result_map: Locator<Tile> = Locator::new(
        cgmath::Vector3::new(params.map_size.x, params.map_size.y, 1),
        params.map_bounding_box,
    );

    let (min_x, min_y, max_x, max_y) = (1, 1, params.map_size.x - 2, params.map_size.y - 2);
    for x in min_x..=max_x {
        for y in min_y..=max_y {
            result_map
                .access_mut(&vec3(x as i32, y as i32, 0))
                .unwrap()
                .space = true;
        }
    }

    split_vertical(
        &mut rng,
        &mut result_map,
        min_x as i32,
        min_y as i32,
        max_x as i32,
        max_y as i32,
        params,
    );

    result_map
}

fn split_vertical(
    rng: &mut Pcg32,
    map: &mut Locator<Tile>,
    min_x: i32,
    min_y: i32,
    max_x: i32,
    max_y: i32,
    params: &MapGenerationParams,
) {
    if min_x < max_x - 1 && min_y < max_y - 1 {
        let pos = rng.gen_range(min_x + 1, max_x);
        let count = rng.gen_range(2, max(3, ((max_y - min_y) as f32 * 0.5) as i32));
        let mut free = vec![];
        for i in 0..count {
            free.push((min_y..=max_y).choose(rng).unwrap());
        }
        for y in (min_y..=max_y).filter(|y| !free.contains(y)) {
            map.access_mut(&vec3(pos, y, 0)).unwrap().space = false;
        }

        split_horizontal(rng, map, min_x, min_y, pos - 1, max_y, params);
        split_horizontal(rng, map, pos + 1, min_y, max_x, max_y, params);
    }
}

fn split_horizontal(
    rng: &mut Pcg32,
    map: &mut Locator<Tile>,
    min_x: i32,
    min_y: i32,
    max_x: i32,
    max_y: i32,
    params: &MapGenerationParams,
) {
    if min_x < max_x - 1 && min_y < max_y - 1 {
        let pos = rng.gen_range(min_y + 1, max_y);
        let count = rng.gen_range(2, max(3, ((max_x - min_x) as f32 * 0.5) as i32));
        let mut free = vec![];
        for i in 0..count {
            free.push((min_x..=max_x).choose(rng).unwrap());
        }
        for x in (min_x..=max_x).filter(|x| !free.contains(x)) {
            map.access_mut(&vec3(x, pos, 0)).unwrap().space = false;
        }

        split_vertical(rng, map, min_x, min_y, max_x, pos - 1, params);
        split_vertical(rng, map, min_x, pos + 1, max_x, max_y, params);
    }
}
