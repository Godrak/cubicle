use super::*;
use crate::game::generation::colors::{ColorPalette, PaletteType};

pub struct CharacterGenerationParams {
    pub seed: u64,
    pub color_seed: u64,
    pub bounding_box: BoundingBox,
    pub position: Vector3<f32>,
}

pub fn generate_character(params: &CharacterGenerationParams, fiber: &mut Fiber) -> EntityId {
    let world_id = fiber
        .access_all_components::<WorldTag>()
        .unwrap()
        .first()
        .unwrap()
        .world_id;

    let character_id = fiber.create_entity();
    let char = Character {
        id: character_id,
        alive: true,
    };
    fiber.set(character_id, char);
    let position = Transformation::new(&params.position);
    let transform_id = fiber.set(character_id, position).unwrap() as u32;

    let mut rgn = Pcg32::seed_from_u64(params.seed);
    let palette = ColorPalette::new(params.color_seed, 6, PaletteType::Random, params.color_seed % 2 == 0);

    let physics_body = PhysicsBody{
        body: params.bounding_box,
        priority: 1,
    };
    fiber.set(character_id, physics_body);

    let total_height = params.bounding_box.size().z;
    let wheel_size = rgn.gen_range(0.2, 0.5) * total_height;
    let eyes_pos = 0.9 * total_height;
    let body_size = (eyes_pos - wheel_size) * 0.8;

    let mut horizontal_center = vec3(0.0, 0.0, 0.0);
    let up = vec3(0.0, 0.0, 1.0);

    let wheel = generate_wheel(&mut rgn, transform_id,
                               horizontal_center + up * (wheel_size / 2.0),
                               wheel_size, params.bounding_box.size().x, &palette.colors[0..2]);
    let body = generate_body(&mut rgn, transform_id,
                             horizontal_center + up * (wheel_size + body_size / 2.0),
                             body_size, params.bounding_box.size().x, &palette.colors[2..4]);

    let right_eye = generate_eye(&mut rgn, transform_id,
                                 horizontal_center + up * (eyes_pos) + vec3(1.0, 0.0, 0.0) * params.bounding_box.size().x / 4.0,
                                 0.2 * total_height, 0.2 * total_height, &palette.colors[4..6]);
    let left_eye = generate_eye(&mut rgn, transform_id,
                                horizontal_center + up * (eyes_pos) + vec3(1.0, 0.0, 0.0) * -params.bounding_box.size().x / 4.0,
                                0.2 * total_height, 0.2 * total_height, &palette.colors[4..6]);

    let vertex_data = fiber.borrow_mut::<VertexData>(world_id).unwrap();
    vertex_data.add_data_local(&wheel);
    vertex_data.add_data_local(&body);
    vertex_data.add_data_local(&right_eye);
    vertex_data.add_data_local(&left_eye);

    return character_id;
}

fn generate_wheel(rgn: &mut Pcg32, transform_id: u32, center: V, height: f32, thickness: f32, colors: &[Color]) -> Vec<Vertex> {
    let thickness = rgn.gen_range(0.2, 0.8) * thickness;
    let normal = vec3(1.0, 0.0, 0.0);
    let template = scale_from_centroid(&create_ring(12, center,
                                                    normal, vec3(0.0, 0.0, 1.0)),
                                       height / 2.0);

    let first = translate(
        &scale_from_centroid(&template, height * rgn.gen_range(0.2, 0.8)),
        -normal * thickness / 2.0);
    let second = translate(&template, -normal * thickness / 4.0);
    let third = translate(&template, normal * thickness / 4.0);
    let fourth = translate(&first, normal * thickness);

    let positions = assemble_mesh(&[first, second, third, fourth], true, true);
    let normals = compute_normals(&positions);

    positions
        .into_iter()
        .zip(normals.into_iter())
        .map(|(p, n)| Vertex::new(transform_id, p, n,
                                  color_to_vec(colors.iter().choose(rgn).unwrap())))
        .collect()
}

fn generate_body(rgn: &mut Pcg32, transform_id: u32, center: V, height: f32, thickness: f32, colors: &[Color]) -> Vec<Vertex> {
    let normal = vec3(0.0, 0.0, 1.0);
    let template = scale_from_centroid(&create_ring_facing_up(12, center, normal), thickness / 2.0);

    let zero = scale_from_centroid(&translate(&template, normal * (-height / 2.0)), 0.2);
    let last = scale_from_centroid(&translate(&template, normal * (height / 2.0)), 0.2);

    let cubic = |a: f32, b: f32, c: f32, d: f32, t: f32|
        a * (1.0 - t) * (1.0 - t) * (1.0 - t) + b * 3.0 * (1.0 - t) * (1.0 - t) * t + c * 3.0 * (1.0 - t) * t * t + d * t * t * t as f32;
    let mut shape = |t: f32| cubic([0.2, 0.4, 0.6, 0.8, 1.0].iter().choose(rgn).unwrap().clone() as f32,
                                   [0.2, 0.4, 0.6, 0.8, 1.0].iter().choose(rgn).unwrap().clone() as f32,
                                   [0.2, 0.4, 0.6, 0.8, 1.0].iter().choose(rgn).unwrap().clone() as f32,
                                   [0.2, 0.4, 0.6, 0.8, 1.0].iter().choose(rgn).unwrap().clone() as f32, t);

    let first = scale_from_centroid(&translate(&template, normal * (-height / 2.01)), shape(0.0) * (thickness / 2.0));
    let second = scale_from_centroid(&translate(&template, normal * (-height / 4.0)), shape(0.25) * (thickness / 2.0));
    let third = scale_from_centroid(&translate(&template, normal * 0.0), shape(0.50) * (thickness / 2.0));
    let fourth = scale_from_centroid(&translate(&template, normal * (height / 4.0)), shape(0.75) * (thickness / 2.0));
    let fifth = scale_from_centroid(&translate(&template, normal * (height / 2.0)), shape(1.0) * (thickness / 2.0));

    let positions = assemble_mesh(&[zero, first, second, third, fourth, fifth, last], true, true);
    let normals = compute_normals(&positions);

    positions
        .into_iter()
        .zip(normals.into_iter())
        .map(|(p, n)| Vertex::new(transform_id, p, n,
                                  color_to_vec(colors.iter().choose(rgn).unwrap())))
        .collect()
}


fn generate_eye(rgn: &mut Pcg32, transform_id: u32, center: V, height: f32, thickness: f32, colors: &[Color]) -> Vec<Vertex> {
    let thickness = rgn.gen_range(0.5, 1.0) * thickness;
    let normal = vec3(0.0, 1.0, 0.0);
    let template = scale_from_centroid(&create_ring(6, center,
                                                    normal, vec3(0.0, 0.0, 1.0)),
                                       height / 2.0);

    let first = translate(
        &scale_from_centroid(&template, height * rgn.gen_range(0.2, 0.8)),
        -normal * thickness / 2.0);
    let second = translate(&template, -normal * thickness / 4.0);
    let third = translate(&template, normal * thickness / 4.0);
    let fourth = translate(&first, normal * thickness);

    let positions = assemble_mesh(&[first, second, third, fourth], true, true);
    let normals = compute_normals(&positions);

    positions
        .into_iter()
        .zip(normals.into_iter())
        .map(|(p, n)| Vertex::new(transform_id, p, n,
                                  color_to_vec(colors.iter().choose(rgn).unwrap())))
        .collect()
}