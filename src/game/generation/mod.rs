extern crate ruring;

pub mod colors;
pub mod map;
pub mod character;

use super::components::*;
use super::geometry::*;
use super::graphics::vertex::Vertex;
use super::graphics::vertex_data::VertexData;
use super::locator::*;
use cgmath::*;
use fiber::{Fiber, EntityId};
use rand::prelude::IteratorRandom;
use rand::Rng;
use rand::SeedableRng;
use rand_pcg::*;
use ruring::prelude::*;
use std::cmp::max;
use crate::game::generation::colors::Color;

pub fn color_to_vec(color: &Color) -> V {
    return vec3(color.red, color.blue, color.green);
}
