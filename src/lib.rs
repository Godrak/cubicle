use seed::*;
use seed::prelude::*;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

mod game;

use game::*;

////////////////////////////////////////////////////////////////
// Const space
////////////////////////////////////////////////////////////////

const APP_NAME: &str = "cubicle";
const GAME_CANVAS: &str = "game_canvas";

////////////////////////////////////////////////////////////////
// Model space
////////////////////////////////////////////////////////////////

fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.stream(streams::window_event(Ev::Resize, |_| { Msg::OnResize }));
    orders.stream(streams::window_event(Ev::KeyDown, |event| {
        event.prevent_default();
        event.stop_propagation();
        Msg::KeyDown(event.unchecked_into())
    }));
    orders.stream(streams::window_event(Ev::KeyUp, |event| {
        event.prevent_default();
        event.stop_propagation();
        Msg::KeyUp(event.unchecked_into())
    }));
    Model {
        canvas_size: get_opt_canvas_size(),
        game: None,
    }
}

struct Model {
    canvas_size: (i32, i32),
    game: Option<Game>,
}

////////////////////////////////////////////////////////////////
// Message space
////////////////////////////////////////////////////////////////

enum Msg {
    StartGame,
    OnResize,
    Update(RenderInfo),
    KeyDown(web_sys::KeyboardEvent),
    KeyUp(web_sys::KeyboardEvent),
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    orders.after_next_render(|render_info| Msg::Update(render_info));

    match msg {
        Msg::StartGame => {
            let (gl, shader_version) = create_rendering_context();
            model.game = Some(Game::new(StartParams::new(
                gl,
                shader_version,
                model.canvas_size,
            )));
        }
        Msg::Update(render_info) => {
            if let Some(game) = model.game.as_mut() {
                game.update(render_info)
            }
        }
        Msg::OnResize => {
            if let Some(game) = model.game.as_mut() {
                let (w, h) = get_opt_canvas_size();
                game.react(Trigger::Resize(w, h));
            }
        }
        Msg::KeyUp(ev) => {
            if let Some(game) = model.game.as_mut() {
                game.react(Trigger::KeyDown(ev.key_code(), false));
            }
        }
        Msg::KeyDown(ev) => {
            if let Some(game) = model.game.as_mut() {
                game.react(Trigger::KeyDown(ev.key_code(), true));
            }
        }
    }
}

fn create_rendering_context() -> (glow::Context, &'static str) {
    let canvas = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id(GAME_CANVAS)
        .unwrap()
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();
    let webgl2_context = canvas
        .get_context("webgl2")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::WebGl2RenderingContext>()
        .unwrap();
    let gl = glow::Context::from_webgl2_context(webgl2_context);
    (gl, "#version 300 es")
}

fn view(model: &Model) -> Vec<Node<Msg>> {
    let (canvas_width, canvas_height) = model.canvas_size;
    vec![
        div![
            C!["main-container"],
            div![C!["MainWindow"],
                canvas![
                attrs! {At::Width => canvas_width, At::Height => canvas_height, At::Id => GAME_CANVAS}
                ]
            ],
            div![C!["Start"],div![C!["neon__button"], "Start Game", ev(Ev::Click, |_| Msg::StartGame)]],
            div![C!["P1"]],
            div![C!["P2"]],
            div![C!["P3"]],
            div![C!["P4"]],
            div![C!["P5"]],
            div![C!["P6"]],
            div![C!["P7"]],
            div![C!["P8"]],

        ]
    ]
}

#[wasm_bindgen(start)]
pub fn main() {
    console_log::init_with_level(log::Level::Trace).unwrap();
    App::start(APP_NAME, init, update, view);
}

// ------ ------
//    Helpers
// ------ ------

fn get_opt_canvas_size() -> (i32, i32) {
    let (mut width, mut height) = window_size();
    width = width.min(height as f64 * 1.5) * 0.9;
    height = width / 1.5;

    (width as i32, height as i32)
}

fn window_size() -> (f64, f64) {
    let window = window();
    let width = window
        .inner_width()
        .expect("window width")
        .unchecked_into::<js_sys::Number>()
        .value_of();
    let height = window
        .inner_height()
        .expect("window height")
        .unchecked_into::<js_sys::Number>()
        .value_of();
    (width, height)
}